#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// 25
void seed_increment(char *seed) {
    if (seed[0]) {
        if (seed[0] == 'z') {
            seed_increment(seed + 1);
            seed[0] = 'a';
        }
        else {
            seed[0]++;
        }
    }
    else {
        seed[1] = 0;
        seed[0] = 'a';
    }
}

// 3 per power of 10
// 4 powers of 10
// 4 groups
// 48 + 4 = 52
void next_num_documents(int *documents, int *groups, int *nodes, char *seed) {
    int max_num = *groups * 1000000;

    if (*documents >= max_num) {
        switch (*groups) {
            case 16:
                *groups = 32;
                *documents = 3200;
                break;

            case 32:
                *groups = 64;
                *documents = 6400;
                break;

            case 64:
                *groups = 128;
                *documents = 12800;
                break;

            case 128:
                seed_increment(seed);
                *groups = 16;
                *documents = 1600;
                break;
        }
    } else {
        int first_digit = (*documents / *groups);
        while (first_digit >= 10)
            first_digit /= 10;

        switch (first_digit) {
            case 1:
            case 5:
                *documents *= 2;
                break;

            case 2:
                *documents = (*documents * 5) / 2;
                break;
        }
    }
}

// 5
void next_node_type(char *node_type, int *documents, int *groups, int *nodes, char *seed) {
    switch (node_type[1]) {
        case 'd':
            node_type[1] = 'l';
            break;

        case 'l':
            node_type[1] = 'q';
            break;

        case 'q':
            node_type[1] = 'r';
            break;

        case 'r':
            node_type[1] = 'b';
            break;

        case 'b':
            next_num_documents(documents, groups, nodes, seed);
            node_type[1] = 'd';
            break;
    }
}

int main(int argc, char *argv[]) {
    int num_machines = 1;
    int which_machine = 0;
    int num_documents = 1600;
    int document_sizes = 1024;
    int num_groups = 16;
    int num_nodes = 64;
    char which_node[3];
    char seed[3];
    FILE *fout;

    if (argc > 2) {
        num_machines = atoi(argv[1]);
        which_machine = atoi(argv[2]);
    }

    strcpy(which_node, "-d");
    strcpy(seed, "a");
    
    fout = fopen("test_options", "w");
    
    for (int i = 0; i < 6500; i++) {
        if (i % num_machines == which_machine)
            fprintf(fout, "%d,%d,%d,%d,%s,%s,%s\n", num_documents, document_sizes, num_groups, num_nodes, "--seed", seed, which_node);
        next_node_type(which_node, &num_documents, &num_groups, &num_nodes, seed);
    }

    fclose(fout);
    return 0;
}