pub type NodeCount = usize;
pub type TimeCount = usize;
pub type RegionCount = usize;

#[derive(Debug, Clone, Copy)]
pub struct Quorum {
    pub tot_honest: NodeCount,
    pub tot_malicious: NodeCount,
    pub tot_last_join: TimeCount,
}

#[derive(Debug, Clone, Copy)]
pub struct Region {
    pub(crate) num_honest: NodeCount,
    pub(crate) num_malicious: NodeCount,
    pub(crate) last_join: TimeCount,
    pub num_nodes_since_last_primary_join: NodeCount,
}