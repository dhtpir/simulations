from random import SystemRandom
import hashlib

defaultcrypto = SystemRandom()

# For now, just defaulting to SHA2 (not SHA3 due to version issues)
def compute_document_ID(document):
    return hashlib.sha256(document).digest()

# The maximum ID possible when using SHA3_256
MAX_ID = 2**256 - 1

# The size of hashes in bytes when using SHA3_256
SIZE_OF_HASH = 16

# "Between" in the circular sense; helps determine what node a document belongs to
def between(test, a, b):
    if a <= test and test < b:
        return True
    if a <= test and b < a:
        return True
    if b < a and test < b:
        return True
    return False

# Just generates some random bytes of a given size
def generate_file(size, cryptogen=defaultcrypto):
	return (''.join([chr(cryptogen.randrange(128)) for i in range(size)])).encode('utf-8')

# This comes from Backes et al., which states that what you'll actually do OT for is
# an AES key to unlock a specific row from the table (and that key will be 256 bits)
# This is a consequence of the Naor and Pinkas construction Backes et al. recommends
SIZE_OF_OT_VALUE = int(256 / 8)

# signature and key sizes taken from https://github.com/poanetwork/threshold_crypto/, which implements BLS signatures and the extension to threshold signatures by Boldyreva
# extensions of BLS signatures to threshold cases are specifically cited in RCP as how they anticipate their threshold signatures to be implemented
# (as far as I can tell, this signature size applies regardless of threshold/size of message, which is not surprising to me)
SIZE_OF_KEY = 48
SIZE_OF_SIGNATURE = 96

# For now, simulating IPv4
SIZE_OF_IP_ADDRESS = 4

# Assuming a timestamp is 32 bits (could easily be updated for Y2k38 problem)
SIZE_OF_TIMESTAMP = 4

# A variable used in test_harness; shouldn't make much of a difference what it's set to,
# as long as it's a positive integer less than the number of nodes in the test
KNOWN_NODE = 0

# 150 ms; assuming wired but transatlantic, this is a conservative estimate
AVERAGE_RTT_ESTIMATE = 0.15 # in s

# 50 Mb/s; global average bandwidth
AVERAGE_CLIENT_BANDWIDTH_ESTIMATE = 50.0 * 1024 * 1024 / 8 # in B/s

# 150 Mb/s; servers estimated with more bandwith
AVERAGE_SERVER_BANDWIDTH_ESTIMATE = 50.0 * 1024 * 1024 / 8 # in B/s

# ~1 GB/s; approximated from 3 cycles / B to encrypt and a 3 GHz machine
ENCRYPTION_SPEED_ESTIMATE = 1000 * 1000 * 1000  # in B/s (yes, 1000, not 1024, because 1GHz is 1000000000 Hz, not 1024^3 Hz)

##
# 4 GB/s; approximated from math Ian and Stan did during a meeting for Shamir PIR
# (more precisely, we calculated it as 250ms/GB, but that's the same value of course)
PIR_SPEED_ESTIMATE = 1024 * 1024 * 1024 / 0.25 # in B/s

# Just what we used in the simulations
SIZE_OF_CHUNK = 1024    # in B

# 99% CI for the error bars
Z_STAR = 2.576
